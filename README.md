# Image Hosting

Image Hosting web made with ExpressJS/NodeJS and Vue. Deployed on Heroku: [image-hosting-backend.herokuapp.com](https://image-hosting-backend.herokuapp.com)


## Setting up the development environment

1. Install the dependencies

```
npm install
```

2. Create a .env file to setup the environment variables used by the REST API, an example of how this file should look like is in [.env.example](.env.example)

3. Start the server in development mode

```
npm run start:dev
```

## API Documentation

### ```POST /api/auth/token```

Will validate the id_token of Google and return a JWT token for future requests to the API.

#### Example JSON Request

```
{ 
   "id_token": "[Google ID Token]"
}
```

#### Example JSON Response

```
{ 
  "token":"[JWT token]",
  "user":{ 
    "id":"000000000000",
    "name":"John Doe",
    "picture":"https://lh6.googleusercontent.com/********/photo.jpg",
    "email_verified":true,
    "email":"john.doe@gmail.com"
  }
}
```

### ```GET /api/auth/user```

Will return the user of the JWT token. Requires header Authorization.

#### Example JSON Request

```
{ 
   "id_token": "[Google ID Token]"
}
```

#### Example JSON Response

```
{
  "token":"[JWT token]",
  "user":{ 
    "id":"000000000000",
    "name":"John Doe",
    "picture":"https://lh6.googleusercontent.com/********/photo.jpg",
    "email_verified":true,
    "email":"john.doe@gmail.com"
  }
}
```

### ```POST /api/picture```

Creates a new picture

#### Example multipart/form-data request

```
{ 
  "name": "My image",
  "file": [File object]
}
```

#### Example JSON response

```
{
  _id: 5d7c6011a41e0a41ee796466,
  name: "My image",
  filename: "165298c91c041fdde7c4bb8dee3790a6",
  createdAt: 2019-09-14T03:35:45.482Z,
  updatedAt: 2019-09-14T03:35:45.482Z,
  __v: 0
}
```

### ```GET /api/picture```

Return all pictures objects from a user

#### Example JSON response

```
{
  pictures: [
    {
      {
        _id: 5d7c6011a41e0a41ee796466,
        name: "My image",
        filename: "165298c91c041fdde7c4bb8dee3790a6",
        createdAt: 2019-09-14T03:35:45.482Z,
        updatedAt: 2019-09-14T03:35:45.482Z,
        __v: 0
      },
      {
        _id: 5d7c8f7529b9be76dfbd827e,
        name: "My old image",
        filename: "18f245b0f208aec5a253fe7f32a1ad5a",
        createdAt: 2019-09-14T06:57:57.945Z,
        updatedAt: 2019-09-14T06:57:57.945Z,
        __v: 0
      }
    }
  ]
}
```

### ```GET /api/picture/1```

Retrieve the picture object of id 1

#### Example JSON response

```
{
  picture:
    {
      _id: 5d7c6011a41e0a41ee796466,
      name: "My image",
      filename: "165298c91c041fdde7c4bb8dee3790a6",
      createdAt: 2019-09-14T03:35:45.482Z,
      updatedAt: 2019-09-14T03:35:45.482Z,
      __v: 0
    }
}
```

### ```GET /api/picture/1/file```

Return the raw picture image file of id 1