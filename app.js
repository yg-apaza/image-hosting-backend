var express = require('express');
var cors = require('cors')
var mongoose = require('mongoose');
const { port, mongodb_uri } = require('./config');

/**
 * APP CONFIGURATION
 */

var app = express();
app.use(cors())
app.use(express.json());

// MongoDB connection
mongoose.connect(mongodb_uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

/**
 * APP ROUTES
 */

app.use(require('./routes'));

app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.json({'errors': [err.message]});
});

/**
 * START SERVER
 */

app.listen(port, () => {
  console.log('Server is running on port', port);
});