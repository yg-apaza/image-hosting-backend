const { google_client_id } = require('.');
const { OAuth2Client } = require( 'google-auth-library' );
var client = new OAuth2Client( google_client_id, '', '' );

module.exports.getGoogleUser = ( code ) => {
  return client.verifyIdToken( { idToken: code, audience: google_client_id } )
    .then( login => {
      var payload = login.getPayload();
      var audience = payload.aud;
      if ( audience !== google_client_id ) {
          throw new Error( "error while authenticating google user: audience mismatch: wanted [" + google_client_id + "] but was [" + audience + "]" )
      }
      return {
        id: payload['sub'],
        name: payload['name'],
        picture: payload['picture'],
        email_verified: payload['email_verified'], 
        email: payload['email']
      }
    })
    .then( user => { return user; } )
    .catch( err => {
      throw new Error( "Error while authenticating google user: " + JSON.stringify( err ) );
    })
}