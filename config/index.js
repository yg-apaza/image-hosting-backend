const dotenv = require('dotenv');

if(process.env.NODE_ENV === 'development') {
  dotenv.config();
}

module.exports = {
  port: process.env.PORT,
  jwt_secret: process.env.JWT_SECRET,
  mongodb_uri: process.env.MONGODB_URI,
  google_client_id: process.env.GOOGLE_CLIENT_ID,
  file_storage_path: process.env.FILE_STORAGE_PATH
};