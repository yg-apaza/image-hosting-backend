var jwt = require( 'jsonwebtoken' );
const { jwt_secret } = require('.');

module.exports.verify = (token) => {
  try {
    return jwt.verify(token, jwt_secret);
  } catch (e) {
    throw new Error('JWT token not verified');
  }
}

module.exports.generateToken = (user) => {
  return jwt.sign(user, jwt_secret);
}