var mongoose = require('mongoose');

var PictureSchema = new mongoose.Schema({
  name: String,
  filename: String,
  author: String
}, {timestamps: true});

mongoose.model('Picture', PictureSchema);
