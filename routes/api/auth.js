var router = require('express').Router();
var googleAuth = require( '../../config/google-auth' );
var jwt = require('../../config/jwt');
var { authMiddleware } = require('../auth-middleware');

router.post('/token', (req, res) => {
  try {
    var login = req.body;
    googleAuth.getGoogleUser(login.token)
      .then(response => {
        var content = {
          token: jwt.generateToken(response),
          user: response
        }
        return content
      })
      .then(credentials => {
        res.json(credentials);
      })
      .catch(e => {
          console.log(e)
          throw new Error(e)
      })
  } catch (error) {
    res.status(500);
    res.json({'errors': ['Internal server error']});
  }
});

router.get('/user', authMiddleware, (req, res) => {
  let principal = res.locals.principal;
  res.json({ user: principal, token: jwt.generateToken(principal)});
});

module.exports = router;