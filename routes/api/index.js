var router = require('express').Router();

router.use('/auth', require('./auth'));
router.use('/picture', require('./picture'))

module.exports = router;