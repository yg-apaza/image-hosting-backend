var router = require('express').Router();
var multer  = require('multer')
var mongoose = require('mongoose');
var path = require('path');
var { authMiddleware } = require('../auth-middleware');
const { file_storage_path } = require('../../config');

var root = path.dirname(require.main.filename)

require('../../models/Picture');
var Picture = mongoose.model('Picture');
var upload = multer({ dest: file_storage_path })

router.get('/', authMiddleware, (req, res) => {
  Picture.find({ author: res.locals.principal.sub }, (err, pictures) => {
    return res.json({ pictures });
  });
})

router.get('/:id', authMiddleware, (req, res) => {
  Picture.findOne({ _id: req.params.id }, (err, picture) => {
    return res.json({ picture });
  });
})

router.get('/:id/file', (req, res) => {
  Picture.findOne({ _id: req.params.id }, (err, picture) => {
    return res.sendFile(path.join(root, file_storage_path, picture.filename))
  });
})

router.post('/', authMiddleware, upload.single('file'), (req, res, next) => {
  var picture = new Picture({
    name: req.body.name,
    filename: req.file.filename,
    author: res.locals.principal.sub
  });

  return picture.save().then(() => {
    return res.json({ picture });
  });
})

module.exports = router;