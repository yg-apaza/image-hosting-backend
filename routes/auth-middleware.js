var jwt = require('../config/jwt');

const checkToken = (req) => {
  let authorization = req.headers.authorization;
  if (!authorization) {
    throw new Error(401)
  }
  let token = authorization.replace('Bearer ', '');
  return jwt.verify(token);
}

module.exports.authMiddleware = (req, res, next) => {
  try {
    let principal = checkToken(req);
    res.locals.principal = principal;
    next();
  } catch (e) {
    res.status(401);
    res.json({errors: ["Not Authorized"]});
  }
}